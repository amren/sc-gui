#sc-gui
#
How to use this user interface
  1. clone this repository to your local machine into some folder
  2. run the command go build inside the folder 
  3. run the generated binary using command ./sc-gui 
	And this will start the server at port ::8080
	go to your browser and open the port address in your browser => 127.0.0.1:8080
  4. You will see a user interface for schema checker
#
