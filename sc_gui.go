/*
This is a GO file to create a gui for the schema checker
This will receive input from the user through GET method
and use that input to run the schema checker with the parameter
and the obtained output will be stored into an ansifilter file
and displayed on the browser.
*/

package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os/exec"
)

var tpl *template.Template
var url string

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}
func index(w http.ResponseWriter, r *http.Request) {
	tpl.ExecuteTemplate(w, "index.html", "Home Page")

	r.ParseForm()
	url = r.FormValue("custom-domain")
	fmt.Printf("%s", url)
	if url == "" {
		log.Println("No URL found")
	} else {
		cmd := exec.Command("./command.sh", url)
		out, err := cmd.CombinedOutput()
		if err != nil {
			log.Fatalf("cmd.Run() failed with %s\n", err)
		}
		fmt.Printf("combined out:\n%s\n", string(out))
		tpl.ExecuteTemplate(w, "output.html", "Schema checker output")
	}

}

func main() {
	local_address := "127.0.0.1:8080"
	http.HandleFunc("/", index)
	http.Handle("/assets/", http.StripPrefix("/assets", http.FileServer(http.Dir("./assets"))))
	fmt.Println("Server listening and running at", local_address)
	http.ListenAndServe(local_address, nil)
}
